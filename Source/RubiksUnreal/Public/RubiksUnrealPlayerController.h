// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "RubiksUnrealPlayerController.generated.h"

/** PlayerController class used to enable cursor */
UCLASS()
class ARubiksUnrealPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARubiksUnrealPlayerController(const FObjectInitializer& ObjectInitializer);
};


