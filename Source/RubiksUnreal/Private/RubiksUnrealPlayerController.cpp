// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "RubiksUnreal.h"
#include "RubiksUnrealPlayerController.h"

ARubiksUnrealPlayerController::ARubiksUnrealPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	//DefaultMouseCursor = EMouseCursor::Crosshairs;
}
