// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "RubiksUnreal.h"
#include "RubiksUnrealGameMode.h"
#include "RubiksUnrealPlayerController.h"

ARubiksUnrealGameMode::ARubiksUnrealGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// no pawn by default
	DefaultPawnClass = NULL;
	// use our own player controller class
	PlayerControllerClass = ARubiksUnrealPlayerController::StaticClass();
}
